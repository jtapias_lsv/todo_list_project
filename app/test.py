from flask_app import app
import unittest

class TodoTestCase(unittest.TestCase):

    def setUp(self):
        """Codes to execute before any test"""
        pass

    def test_index(self):
        """Ensure that flask was set up correctly"""
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
    
    def test_about_page_load(self):
        "Ensure that about page loads correctly"
        tester = app.test_client(self)
        response = tester.get('/about', content_type='html/text')
        self.assertTrue(b'about us' in response.data)

    def tearDown(self):
        """Code to execute after all test"""
        pass

if __name__ == '__main__':
    unittest.main()