import socket
import time
import os

# script to check when the database is ready to connect

port = int(os.environ["DATABASE_PORT"])

socket_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        socket_connection.connect(('database', port))
        socket_connection.close()
        print('********* Database Is Ready Now **********')
        break
    except socket.error as ex:
        time.sleep(0.1)