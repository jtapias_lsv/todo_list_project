from flask_app import app
from flask_app.utils.db import db
from flask_app.config import debug_mode

# to create the tables in database from models
with app.app_context():
    db.create_all()

if __name__ == '__main__':
    app.run(debug=debug_mode)
