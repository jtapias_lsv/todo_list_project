#! /bin/sh

# to skip in errors
set -e

# to check if database is ready
python db_ready.py

# running uwsgi with app.ini file configutarion 
uwsgi app.ini