from flask import Flask
from flask_app.views.todos import todos
from flask_app.utils.db import db
from flask_app.config import DATABASE_CONNECTION_URI, SECRET_KEY

app = Flask(__name__)

app.secret_key = SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_CONNECTION_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

app.register_blueprint(todos)