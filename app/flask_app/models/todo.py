from flask_app.utils.db import db

class Todo(db.Model):
    """Model to persisting the data in a database"""

    __tablename__ =  'todo'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(100), nullable=False)
    complete = db.Column(db.Boolean, default=False)

    def __init__(self, description, complete=False):
        self.description = description
        self.complete = complete