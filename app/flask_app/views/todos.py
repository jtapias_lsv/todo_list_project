from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_app.models.todo import Todo
from flask_app.utils.db import db
from flask_app.config import app_name

todos = Blueprint('todos', __name__)

@todos.route("/hello")
def hello():
    """route for testing the environment variables"""

    name = app_name
    
    if app_name:
        return f"Hello from {name} running in a Docker container behind Nginx! change"

    return "Hello from Flask"

@todos.route('/')
def index():
    """list all todo records"""

    todos = Todo.query.all()
    return render_template('index.html', todos=todos)


@todos.route('/new', methods=['POST'])
def add_todo():
    """create a new todo record"""
    
    description = request.form.get('description')
    complete = True if request.form.get('complete') else False
    todo = Todo(description, complete)
    db.session.add(todo)
    
    db.session.commit()
    
    flash('Todo created successfully')
    
    return redirect(url_for('todos.index'))


@todos.route('/update/<id>', methods=['GET', 'POST'])
def update_todo(id):
    """update a todo with a specific id"""

    todo = Todo.query.get(id)
    if request.method == 'POST':
        todo.description = request.form.get('description')
        todo.complete = True if request.form.get('complete') else False
        
        db.session.commit()
        
        flash('Todo updated successfully')
        
        return redirect(url_for('todos.index'))
    return render_template('update.html', todo=todo)


@todos.route('/delete/<id>')
def delete_todo(id):
    """drop a todo record with specific id"""

    todo = Todo.query.get(id)
    db.session.delete(todo)

    db.session.commit()
    
    flash('Todo deleted successfully')

    return redirect(url_for('todos.index'))


@todos.route('/about')
def about():
    """about this project"""

    return render_template('about.html')
