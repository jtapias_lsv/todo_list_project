import os

# Use os.getenv("key") to get environment variables from the docker-compose file
app_name = os.environ.get("APP_NAME")
db_user = os.environ.get('DATABASE_USER')
db_pass = os.environ.get('DATABASE_PASSWORD')
db_host = os.environ.get('DATABASE_HOST')
db_port = os.environ.get('DATABASE_PORT')
db_name = os.environ.get('DATABASE_NAME')
secret_key = os.environ.get('SECRET_KEY')
debug_mode = bool(int(os.environ.get('DEBUG_MODE')))

DATABASE_CONNECTION_URI = f'postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}'
SECRET_KEY = secret_key
