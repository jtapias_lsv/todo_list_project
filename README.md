# TODO List Project

### A docker flask app with proxy, uwsgi and database.

This project is a practice for the docker learning courses, in the ZINA Seniority Road Map.

The project tries to shows all knowleged of docker and python applications, is still in development status although is trying to keep the best practices for coding and configurations.

The project consist in a flask app dockerized usisng three services:

- **App**: Python Flask with SQLAlquemy
- **Proxy**: Nginx    
- **Database**: Postgresql

## Author

- [@jtapias_lsv](https://www.gitlab.com/jtapias_lsv)


## To run the project:

Clone the repository:

```console
git clone git@gitlab.com:jtapias_lsv/todo_list_project.git
```
Chage directory to the project folder
```console
cd todo_list_project
```

Create an  **.env**   file:

```console
cp .env.sample .env
```
Within **.env** file replace all variables example values with apropiate values (variables for your project).

Ensure your port **80** is free to use.

Run the project with:

```console
docker-compose up --build -d
```
Go to the browser and copy this url:

- [localhost](http://127.0.0.1)


The application is configured with volumes to keep the data, after running and registing some records, (see [using the app section](#using-the-app) ) you can stop the project:

```console
docker-compose stop
```

After yo can restart the application and the data is persisting:

```console
docker-compose start
```

Try for your self.


## Lessons Learned

What did you learn while building this project?

- Docker  best practices
- Flask with an ORM

What challenges did you face and how did you overcome them?

- How to build my first dockerized app
- How to set variables environments
- How to create virtual environtment in containers
- How to create an appropiate documentation


## Using the app

The app is CRUD application.

To know about the use of application please check the about page of app.


## Feedback

If you have any feedback, please reach out to us at jose.tapias.ext@nokia.com

