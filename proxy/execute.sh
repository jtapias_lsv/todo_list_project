#!/bin/sh

# to skip in errors
set -e

# for change environment variable values that match with name in ${} syntax
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/http.d/default.conf

# stating the ngnix server
nginx -g 'daemon off;'